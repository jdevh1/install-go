#!/bin/bash

DEV=$1
INSTALL_PATH=$2
GO_VERSION=1.16.4
GO_DISTRO=linux-amd64

GO_ARCHIVE=go$GO_VERSION.$GO_DISTRO.tar.gz
GO_ABS_PATH=$INSTALL_PATH/go

wget https://dl.google.com/go/$GO_ARCHIVE

rm -rf $GO_ABS_PATH
tar -xzvf $GO_ARCHIVE -C $INSTALL_PATH

gopath=/home/$DEV/go
mkdir $gopath $gopath/src $gopath/pkg $gopath/bin

echo "" >> /etc/profile
echo "#BEGIN_GO_INSTALL" >> /etc/profile
echo "export PATH=\$PATH:$GO_ABS_PATH/bin" >> /etc/profile
echo "export GOPATH=$gopath" >> /etc/profile
echo "#END_GO_INSTALL" >> /etc/profile
echo "" >> /etc/profile

chown -R $DEV:$DEV $gopath
rm $GO_ARCHIVE

source /etc/profile
